LFILE="/etc/lsb-release"
MFILE="/System/Library/CoreServices/SystemVersion.plist"
if [[ -f $LFILE ]]; then
    _distro=$(awk '/^ID=/' /etc/*-release | awk -F'=' '{ print tolower($2) }')
    echo "$_distro"
elif [[ -f $MFILE ]]; then
    _distro="macos"
fi