function New-Symlink {
    param (
        [string] $Source,
        [string] $Target
    )
    New-Item -ItemType SymbolicLink -Target $Source -Path $Target -Force | Out-Null
}

function Test-Elevated {
    # Get the ID and security principal of the current user account
    $myIdentity = [ System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myIdentity)
    # Check to see if we are currently running "as Administrator"
    return $myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}

# Check to see if we are currently running "as Administrator"
if (!(Test-Elevated)) {
    Write-Host "[-] Administrator privilege required." -ForegroundColor "Red"
    $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell"
    $newProcess.Arguments = $myInvocation.MyCommand.Definition
    $newProcess.Verb = "runas"
    [System.Diagnostics.Process]::Start($newProcess)
 
    exit
}

$CurrentPath = Split-Path $PSCommandPath
# powershell profile
$profileDir = Split-Path -parent $profile
New-Item $profileDir -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
## symlinks
Write-Host "[o] Configuring Powershell..." -ForegroundColor "Magenta"
$Files = @("aliases.ps1", "functions.ps1", "profile.ps1")
$Files | ForEach-Object { 
    New-Symlink -Source (Join-Path -Path $CurrentPath -ChildPath $_) -Target (Join-Path -Path $profileDir -ChildPath $_) 
}
Write-Host "[+] Powershell configured." -ForegroundColor "Green"

$ParentPath = Split-Path -Path $CurrentPath -Parent
$ParentConfig = $ParentPath | Join-Path -ChildPath "config"
# bat
Write-Host "[o] Configuring bat..." -ForegroundColor "Magenta"
if (Get-Command bat.exe -ErrorAction SilentlyContinue | Test-Path) {
    $Source = $ParentConfig | Join-Path -ChildPath "bat.config"
    $Target = bat --config-file
    if ($Source | Test-Path) {
        New-Symlink -Source $Source -Target $Target
        Write-Host "[+] Bat configured." -ForegroundColor "Green"
    }
}
else {
    Write-Host "[!] Bat does not exist." -ForegroundColor "Blue"
}

# starship
Write-Host "[o] Configuring Starship..." -ForegroundColor "Magenta"
if (Get-Command starship.exe -ErrorAction SilentlyContinue | Test-Path) {
    $Target = $env:STARSHIP_CONFIG
    if ($null -eq $Target) {
        $ConfigDir = $env:USERPROFILE | Join-Path -ChildPath ".config"
        New-Item $ConfigDir -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
        $Target = $ConfigDir | Join-Path -ChildPath "starship.toml"
    }
    $Source = $ParentConfig | Join-Path -ChildPath "starship.toml"
    if ($Source | Test-Path) {
        New-Symlink -Source $Source -Target $Target
        Write-Host "[+] Starship configured." -ForegroundColor "Green"
    }
}
else {
    Write-Host "[!] Starship does not exist." -ForegroundColor "Blue"
}

# vscode
Write-Host "[o] Configuring VSCode..." -ForegroundColor "Magenta"
if (Get-Command code -ErrorAction SilentlyContinue | Test-Path) {
    $Target = $env:APPDATA | Join-Path -ChildPath "Code" | Join-Path -ChildPath "User" | Join-Path -ChildPath "settings.json"
    $Source = $ParentConfig | Join-Path -ChildPath "vscode" | Join-Path -ChildPath "settings.json"
    if ($Source | Test-Path) {
        New-Symlink -Source $Source -Target $Target
        Write-Host "[+] VSCode configured." -ForegroundColor "Green"
    }
}
else {
    Write-Host "[!] VSCode does not exist." -ForegroundColor "Blue"
}

# windows terminal
Write-Host "[o] Configuring Windows Terminal..." -ForegroundColor "Magenta"
if (Get-Command wt.exe -ErrorAction SilentlyContinue | Test-Path) {
    $Target = $env:LOCALAPPDATA | Join-Path -ChildPath "Packages" | Join-Path -ChildPath "Microsoft.WindowsTerminal_8wekyb3d8bbwe" `
    | Join-Path -ChildPath "LocalState" | Join-Path -ChildPath "settings.json"
    $Source = $CurrentPath | Join-Path -ChildPath "windows-terminal" | Join-Path -ChildPath "settings.json"
    if ($Source | Test-Path) {
        New-Symlink -Source $Source -Target $Target
        Write-Host "[+] Windows Terminal configured." -ForegroundColor "Green"
    }
}
else {
    Write-Host "[!] Windows Terminal does not exist." -ForegroundColor "Blue"
}

# taskbarx
Write-Host "[o] Configuring TaskbarX..." -ForegroundColor "Magenta"
$Task = Get-ScheduledTask -ErrorAction SilentlyContinue | Where-Object TaskName -CMatch "TaskbarX"
if ($null -ne $Task) {
    $Settings = $CurrentPath | Join-Path -ChildPath "taskbarx" | Join-Path -ChildPath "settings.json"
    $Arguments = @{}
    ([PSCustomObject] (Get-Content $Settings | ConvertFrom-Json).psobject).Properties | ForEach-Object { $Arguments[$_.Name] = $_.Value }
    $TaskArguments = ($Arguments.GetEnumerator() | ForEach-Object { "-$($_.Key)=$($_.Value)" }) -join ' '
    $TaskAction = New-ScheduledTaskAction -Execute $Task.Actions.Execute -Argument $TaskArguments
    Stop-ScheduledTask -TaskName $Task.TaskName
    Set-ScheduledTask -TaskName $Task.TaskName -Action $TaskAction | Out-Null
    Start-ScheduledTask -TaskName $Task.TaskName

    Write-Host "[+] TaskbarX configured." -ForegroundColor "Green"
}
else {
    Write-Host "[!] TaskbarX does not exist." -ForegroundColor "Blue"
}