${function:~} = { Set-Location ~ }

# Missing Bash aliases
Set-Alias time Measure-Command

# ls
if (Get-Command ls.exe -ErrorAction SilentlyContinue | Test-Path) {
    Remove-Item alias:ls -ErrorAction SilentlyContinue;
    # Set `ls` to call `ls.exe` and always use --color
    ${function:ls} = { ls.exe --color @args }
    # List all files in long format
    ${function:l} = { ls -lF @args }
    # List all files in long format, including hidden files
    ${function:la} = { ls -laF @args }
    # List only directories
    ${function:lsd} = { Get-ChildItem -Directory -Force @args }
}
else {
    # List all files, including hidden files
    ${function:la} = { ls -Force @args }
    # List only directories
    ${function:lsd} = { Get-ChildItem -Directory -Force @args }
}

# curl: Use `curl.exe` if available
if (Get-Command curl.exe -ErrorAction SilentlyContinue | Test-Path) {
    Remove-Item alias:curl -ErrorAction SilentlyContinue
    # Set `curl` to call `curl.exe`
    ${function:curl} = { curl.exe @args }
    # Gzip-enabled `curl`
    ${function:gurl} = { curl --compressed @args }
}
else {
    # Gzip-enabled `curl`
    ${function:gurl} = { curl -TransferEncoding GZip }
}

# cat: Use `bat.exe` if available
if (Get-Command bat.exe -ErrorAction SilentlyContinue | Test-Path) {
    Remove-Item alias:cat -ErrorAction SilentlyContinue
    # set `cat` to call `bat.exe`
    ${function:cat} = { bat.exe --paging=never @args }
}

# Reload the shell
Set-Alias reload Reload-Powershell
