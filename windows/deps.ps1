function Test-Installed {
    param (
        [string] $command
    )
    return $null -ne (Get-Command -Name $command -ErrorAction SilentlyContinue);
}

function Get-Choice {
    param (
        [string] $caption,
        [string] $message,
        [string[]] $choices,
        [int] $defaultChoice = 1
    )
    return $Host.UI.PromptForChoice($caption, $message, $choices, $defaultChoice);
}

function Test-Elevated {
    # Get the ID and security principal of the current user account
    $myIdentity = [ System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myIdentity)
    # Check to see if we are currently running "as Administrator"
    return $myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}

function Install-Packages {
    param (
        [string[]] $packages
    )
    foreach ($package in $packages) {
        choco install $package --limit-output
    }
}

function Update-Environment {
    $locations = 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
    'HKCU:\Environment'

    $locations | ForEach-Object {
        $k = Get-Item $_
        $k.GetValueNames() | ForEach-Object {
            $name = $_
            $value = $k.GetValue($_)
            Set-Item -Path Env:\$name -Value $value
        }
    }

    $env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User")
}

# Check to see if we are currently running "as Administrator"
if (!(Test-Elevated)) {
    Write-Error -Message "Please run this script as administrator";

    exit
}

if (!(Test-Installed "choco")) {
    Write-Error -Message "Chocolatey is not installed";

    exit
}

$message = "Would you like to install these packages ?";
$choices = @('&Yes', '&No');
$caption = "The following packages will be installed: ";

# system and cli
$apps = @(
    'curl',
    'git.install -params "/GitAndUnixToolsOnPath /NoShellIntegration"',
    'bat',
    'python',
    'rust',
    'starship'
);
$tmp = $caption + (($apps | ForEach-Object { $_.Split(' ')[0] }) -Join ', ');
if ((Get-Choice $tmp $message $choices) -eq 0) {
    Write-Host "[o] Installing packages..." -ForegroundColor "cyan";
    Install-Packages $apps;
}

# browsers
$browsers = @(
    'Firefox'
);
$tmp = $caption + (($browsers | ForEach-Object { $_.Split(' ')[0] }) -Join ', ');
if ((Get-Choice $tmp $message $choices) -eq 0) {
    Write-Host "[o] Installing packages..." -ForegroundColor "cyan";
    Install-Packages $browsers;
}

# dev tools
$devtools = @(
    'vscode'
);
$tmp = $caption + (($devtools | ForEach-Object { $_.Split(' ')[0] }) -Join ', ');
if ((Get-Choice $tmp $message $choices) -eq 0) {
    Write-Host "[o] Installing packages..." -ForegroundColor "cyan";
    Install-Packages $devtools;
}

# utilities
$utilities = @(
    'vlc',
    '7zip.install'
);
$tmp = $caption + (($utilities | ForEach-Object { $_.Split(' ')[0] }) -Join ', ');
if ((Get-Choice $tmp $message $choices) -eq 0) {
    Write-Host "[o] Installing packages..." -ForegroundColor "cyan";
    Install-Packages $utilities;
}

# customization
$custom = @(
    'taskbarx'
    #'lively' buggy
);
$tmp = $caption + (($custom | ForEach-Object { $_.Split(' ')[0] }) -Join ', ') + ', flow-launcher';
if ((Get-Choice $tmp $message $choices) -eq 0) {
    Write-Host "[o] Installing packages..." -ForegroundColor "cyan";
    Install-Packages $custom;
    # install flow.launcher
    if (!(Test-Installed "winget")) {
        Write-Warning -Message "'winget' is not installed consequently 'flow-launcher' cannot be installed.";
    }
    else {
        winget install "Flow Launcher";
    }
}

Update-Environment