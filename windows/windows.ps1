function Test-Elevated {
    # Get the ID and security principal of the current user account
    $myIdentity = [ System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myIdentity)
    # Check to see if we are currently running "as Administrator"
    return $myPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}

function Get-Choice {
    param (
        [string] $caption,
        [string] $message,
        [string[]] $choices,
        [int] $defaultChoice = 1
    )
    return $Host.UI.PromptForChoice($caption, $message, $choices, $defaultChoice)
}

# Check to see if we are currently running "as Administrator"
if (!(Test-Elevated)) {
    $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell"
    $newProcess.Arguments = $myInvocation.MyCommand.Definition
    $newProcess.Verb = "runas"
    [System.Diagnostics.Process]::Start($newProcess)
 
    exit
}

$message = "Would you like to install these packages ?"
$choices = @('&Yes', '&No')
$caption = "The following packages will be installed: "

###############################################################################
### Security                                                                  #
###############################################################################
$message = "Do you want to make Windows safer?"
$caption = "You can turn on some security features on Windows."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring System..." -ForegroundColor "Cyan"
    
    # Configure the firewall
    Set-NetFirewallProfile -Profile Domain, Public, Private -Enabled True # enable all firewall profiles
    Write-Host "[+] Firewall enabled." -ForegroundColor "Green"

    # disable some services
    $services = @("fax", "iphlpsvc", "RemoteRegistry", "p2pimsvc", "pnrpsvc", "p2psvc", "LanmanServer", "seclogon",
        "UmRdpService", "RasAuto", "PNRPAutoReg", "NetTcpPortSharing", "MSiSCSI", "Mcx2Svc", "lltdsvc",
        "SharedAccess", "HomeGroupProvider", "HomeGroupListener", "PeerDistSvc", "bthserv", "TlntSvr",
        "AJRouter", "BthHFSrv", "Browser", "CDPSvc", "DiagTrack", "lfsvc", "irmon", "wlidsvc",
        "Netlogon", "NcdAutoSetup", "PhoneSvc", "RetailDemo", "RemoteAccess", "TapiSrv", "WinRM",
        "workfolderssvc", "XblAuthManager", "XblGameSave", "XboxNetApiSvc", "fdPHost", "FDResPub",
        "WMPNetworkSvc", "upnphost")
    $existingServices = $services | Where-Object { (Get-Service -Name $_) -ne $null }
    $existingServices | ForEach-Object { Stop-Service $_ -Force; Set-Service $_ -StartupType Disabled }
    Write-Host "[+] Insecure services disabled." -ForegroundColor "Green"

    # enable secure services
    $services = @("wuauserv", "MpsSvc", "EventLog")
    $existingServices = $services | Where-Object { (Get-Service -Name $_) -ne $null }
    $existingServices | ForEach-Object { Start-Service $_ -Force; Set-Service $_ -StartupType Automatic }
    Write-Host "[+] Secure services enabled." -ForegroundColor "Green"

    # UAC
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorAdmin -PropertyType DWord -Value 2 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorUser -PropertyType DWord -Value 3 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name PromptOnSecureDesktop -PropertyType DWord -Value 1 -Force
    Write-Host "[+] UAC enabled." -ForegroundColor "Green"


}

###############################################################################
### Privacy                                                                   #
###############################################################################
$message = "Do you want to shut up Windows?"
$caption = "You can disable things that do not respect privacy."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring privacy (thanks Sophia)..." -ForegroundColor "Cyan"
    
    # Disable the "Connected User Experiences and Telemetry" service (DiagTrack), and block the connection for the Unified Telemetry Client Outbound Traffic
    ## Connected User Experiences and Telemetry
    Get-Service -Name DiagTrack | Stop-Service -Force
    Get-Service -Name DiagTrack | Set-Service -StartupType Disabled
    ## Block connection for the Unified Telemetry Client Outbound Traffic
    Get-NetFirewallRule -Group DiagTrack | Set-NetFirewallRule -Enabled False -Action Block
    Write-Host "[+] DiagTrack disabled." -ForegroundColor "Green"

    # Set the diagnostic data collection to minimum
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection -Name AllowTelemetry -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection -Name AllowTelemetry -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection -Name AllowTelemetry -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection -Name LimitEnhancedDiagnosticDataWindowsAnalytics -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection -Name MaxTelemetryAllowed -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Diagnostics\DiagTrack -Name ShowedToastAtLevel -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Data collection disabled." -ForegroundColor "Green"

    # Turn off the Windows Error Reporting
    if ((Get-WindowsEdition -Online).Edition -notmatch "Core") {
        Get-ScheduledTask -TaskName QueueReporting | Disable-ScheduledTask
        New-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\Windows Error Reporting" -Name Disabled -PropertyType DWord -Value 1 -Force
        New-ItemProperty -Path "HKCU:\Software\Policies\Microsoft\Windows\Windows Error Reporting" -Name Disabled -PropertyType DWord -Value 1 -Force
        New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\Windows Error Reporting\Consent" -Name DefaultConsent -PropertyType DWord -Value 0 -Force
        New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\Windows Error Reporting\Consent" -Name DefaultOverrideBehavior -PropertyType DWord -Value 1 -Force
        New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\Windows Error Reporting" -Name DontSendAdditionalData -PropertyType DWord -Value 1 -Force
        New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\Windows Error Reporting" -Name LogginDisabled -PropertyType DWord -Value 1 -Force
    }

    Get-Service -Name WerSvc | Stop-Service -Force
    Get-Service -Name WerSvc | Set-Service -StartupType Disabled
    Get-Service -Name WerCplSupport | Stop-Service -Force
    Get-Service -Name WerCplSupport | Set-Service -StartupType Disabled
    Write-Host "[+] Error reporting disabled." -ForegroundColor "Green"

    # Change the feedback frequency to "Never"
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Microsoft\Siuf\Rules)) {
        New-Item -Path HKCU:\SOFTWARE\Microsoft\Siuf\Rules -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Siuf\Rules -Name NumberOfSIUFInPeriod -PropertyType DWord -Value 0 -Force
    Remove-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Siuf\Rules -Name PeriodInNanoSeconds -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection -Name DoNotShowFeedbackNotifications -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Policies\Microsoft\Windows\DataCollection -Name DoNotShowFeedbackNotifications -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Feedback disabled." -ForegroundColor "Green"
    
    # Turn off the diagnostics tracking scheduled tasks
    [string[]]$CheckedScheduledTasks = @(
        # Collects program telemetry information if opted-in to the Microsoft Customer Experience Improvement Program
        "ProgramDataUpdater",

        # This task collects and uploads autochk SQM data if opted-in to the Microsoft Customer Experience Improvement Program
        "Proxy",

        # If the user has consented to participate in the Windows Customer Experience Improvement Program, this job collects and sends usage data to Microsoft
        "Consolidator",

        # The USB CEIP (Customer Experience Improvement Program) task collects Universal Serial Bus related statistics and information about your machine and sends it to the Windows Device Connectivity engineering group at Microsoft
        "UsbCeip",

        # The Windows Disk Diagnostic reports general disk and system information to Microsoft for users participating in the Customer Experience Program
        "Microsoft-Windows-DiskDiagnosticDataCollector",

        # This task shows various Map related toasts
        "MapsToastTask",

        # This task checks for updates to maps which you have downloaded for offline use
        "MapsUpdateTask",

        # Initializes Family Safety monitoring and enforcement
        "FamilySafetyMonitor",

        # Synchronizes the latest settings with the Microsoft family features service
        "FamilySafetyRefreshTask",

        # XblGameSave Standby Task
        "XblGameSaveTask"
    )

    # Check if device has a camera
    $DeviceHasCamera = Get-CimInstance -ClassName Win32_PnPEntity | Where-Object -FilterScript { (($_.PNPClass -eq "Camera") -or ($_.PNPClass -eq "Image")) -and ($_.Service -ne "StillCam") }
    if (-not $DeviceHasCamera) {
        # Windows Hello
        $CheckedScheduledTasks += "FODCleanupTask"
    }
    $CheckedScheduledTasks | Disable-ScheduledTask
    Write-Host "[+] Scheduled tasks disabled." -ForegroundColor "Green"

    # Do not let websites provide locally relevant content by accessing language list
    New-ItemProperty -Path "HKCU:\Control Panel\International\User Profile" -Name HttpAcceptLanguageOptOut -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Access to language list disabled." -ForegroundColor "Green"

    # Do not allow apps to use advertising ID to make ads more interresting to you based on your app usage 
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo)) {
        New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo -Name Enabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo -Name DisabledByGroupPolicy -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Use of advertising ID disabled." -ForegroundColor "Green"

    # Hide the Windows welcome experiences after updates and occasionally when I sign in to highlight what's new and suggested
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SubscribedContent-310093Enabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Hide the Windows welcome experience." -ForegroundColor "Green"

    # Get tips, tricks, and suggestions as you use Windows (default value)
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SubscribedContent-338389Enabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Windows tips disabled." -ForegroundColor "Green"

    # Hide from me suggested content in the Settings app
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SubscribedContent-338393Enabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SubscribedContent-353694Enabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SubscribedContent-353696Enabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Suggested content disabled." -ForegroundColor "Green"

    # Turn off automatic installing suggested apps
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager -Name SilentInstalledAppsEnabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Automatic installing suggested apps disabled." -ForegroundColor "Green"

    # Do not suggest ways I can finish setting up my device to get the most out of Windows
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\UserProfileEngagement)) {
        New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\UserProfileEngagement -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\UserProfileEngagement -Name ScoobeSystemSettingEnabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] What's new in windows disabled." -ForegroundColor "Green"

    # Do not let Microsoft offer you tailored expereinces based on the diagnostic data setting you have chosen
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Privacy -Name TailoredExperiencesWithDiagnosticDataEnabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Tailored experiences disabled." -ForegroundColor "Green"

    # Disable Bing search in the Start Menu
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer)) {
        New-Item -Path HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer -Name DisableSearchBoxSuggestions -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search -Name BingSearchEnabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Bing Search disabled." -ForegroundColor "Green"

    # Uninstall OneDrive. The OneDrive user folder won't be removed
    [string]$UninstallString = Get-Package -Name "Microsoft OneDrive" -ProviderName Programs -ErrorAction Ignore | ForEach-Object -Process { $_.Meta.Attributes["UninstallString"] }
    if ($UninstallString) {
        Write-Information -MessageData "" -InformationAction Continue
        Write-Verbose -Message $Localization.OneDriveUninstalling -Verbose

        Stop-Process -Name OneDrive -Force -ErrorAction Ignore
        Stop-Process -Name OneDriveSetup -Force -ErrorAction Ignore
        Stop-Process -Name FileCoAuth -Force -ErrorAction Ignore

        # Getting link to the OneDriveSetup.exe and its' argument(s)
        [string[]]$OneDriveSetup = ($UninstallString -Replace ("\s*/", ",/")).Split(",").Trim()
        if ($OneDriveSetup.Count -eq 2) {
            Start-Process -FilePath $OneDriveSetup[0] -ArgumentList $OneDriveSetup[1..1] -Wait
        }
        else {
            Start-Process -FilePath $OneDriveSetup[0] -ArgumentList $OneDriveSetup[1..2] -Wait
        }

        # Get the OneDrive user folder path and remove it if it doesn't contain any user files
        if (Test-Path -Path $env:OneDrive) {
            if ((Get-ChildItem -Path $env:OneDrive -ErrorAction Ignore | Measure-Object).Count -eq 0) {
                Remove-Item -Path $env:OneDrive -Recurse -Force -ErrorAction Ignore

                # https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-movefileexa
                # The system does not move the file until the operating system is restarted
                # The system moves the file immediately after AUTOCHK is executed, but before creating any paging files
                $Signature = @{
                    Namespace        = "WinAPI"
                    Name             = "DeleteFiles"
                    Language         = "CSharp"
                    MemberDefinition = @"
public enum MoveFileFlags
{
	MOVEFILE_DELAY_UNTIL_REBOOT = 0x00000004
}
[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
static extern bool MoveFileEx(string lpExistingFileName, string lpNewFileName, MoveFileFlags dwFlags);
public static bool MarkFileDelete (string sourcefile)
{
	return MoveFileEx(sourcefile, null, MoveFileFlags.MOVEFILE_DELAY_UNTIL_REBOOT);
}
"@
                }

                # If there are some files or folders left in %OneDrive%
                if ((Get-ChildItem -Path $env:OneDrive -ErrorAction Ignore | Measure-Object).Count -ne 0) {
                    if (-not ("WinAPI.DeleteFiles" -as [type])) {
                        Add-Type @Signature
                    }

                    try {
                        Remove-Item -Path $env:OneDrive -Recurse -Force -ErrorAction Stop
                    }
                    catch {
                        # If files are in use remove them at the next boot
                        Get-ChildItem -Path $env:OneDrive -Recurse -Force | ForEach-Object -Process { [WinAPI.DeleteFiles]::MarkFileDelete($_.FullName) }
                    }
                }
            }
            else {
                Start-Process -FilePath explorer -ArgumentList $env:OneDrive
            }
        }

        Remove-ItemProperty -Path HKCU:\Environment -Name OneDrive, OneDriveConsumer -Force -ErrorAction Ignore
        Remove-Item -Path HKCU:\SOFTWARE\Microsoft\OneDrive -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path "$env:ProgramData\Microsoft OneDrive" -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path $env:SystemDrive\OneDriveTemp -Recurse -Force -ErrorAction Ignore
        Unregister-ScheduledTask -TaskName *OneDrive* -Confirm:$false -ErrorAction Ignore

        # Getting the OneDrive folder path
        $OneDriveFolder = Split-Path -Path (Split-Path -Path $OneDriveSetup[0] -Parent)

        # Save all opened folders in order to restore them after File Explorer restarting
        Clear-Variable -Name OpenedFolders -Force -ErrorAction Ignore
        $Script:OpenedFolders = { (New-Object -ComObject Shell.Application).Windows() | ForEach-Object -Process { $_.Document.Folder.Self.Path } }.Invoke()

        # Terminate the File Explorer process
        New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoRestartShell -PropertyType DWord -Value 0 -Force
        Stop-Process -Name explorer -Force
        Start-Sleep -Seconds 3
        New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoRestartShell -PropertyType DWord -Value 1 -Force

        # Attempt to unregister FileSyncShell64.dll and remove
        $FileSyncShell64dlls = Get-ChildItem -Path "$OneDriveFolder\*\FileSyncShell64.dll" -Force
        foreach ($FileSyncShell64dll in $FileSyncShell64dlls.FullName) {
            Start-Process -FilePath regsvr32.exe -ArgumentList "/u /s $FileSyncShell64dll" -Wait
            Remove-Item -Path $FileSyncShell64dll -Force -ErrorAction Ignore

            if (Test-Path -Path $FileSyncShell64dll) {
                if (-not ("WinAPI.DeleteFiles" -as [type])) {
                    Add-Type @Signature
                }

                # If files are in use remove them at the next boot
                Get-ChildItem -Path $FileSyncShell64dll -Recurse -Force | ForEach-Object -Process { [WinAPI.DeleteFiles]::MarkFileDelete($_.FullName) }
            }
        }

        Start-Sleep -Seconds 1

        # Start the File Explorer process
        Start-Process -FilePath explorer

        # Restoring closed folders
        foreach ($OpenedFolder in $OpenedFolders) {
            if (Test-Path -Path $OpenedFolder) {
                Start-Process -FilePath explorer -ArgumentList $OpenedFolder
            }
        }

        Remove-Item -Path $OneDriveFolder -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path $env:LOCALAPPDATA\OneDrive -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path $env:LOCALAPPDATA\Microsoft\OneDrive -Recurse -Force -ErrorAction Ignore
        Remove-Item -Path "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\OneDrive.lnk" -Force -ErrorAction Ignore
    }
    Write-Host "[+] OneDrive uninstalled." -ForegroundColor "Green"

    # Disable Application launch tracking
    New-ItemProperty -Path HKCU:\\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name "Start-TrackProgs" -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Application launch tracking disabled." -ForegroundColor "Green"

    # Disable Smartscreen
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\Windows\System -Name EnableSmartScreen -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer -Name SmartScreenEnabled -PropertyType String -Value Off -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Explorer -Name SmartScreenEnabled -PropertyType String -Value Off -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\Windows\System -Name ShellSmartScreenLevel -PropertyType String -Value Warn -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\Edge -Name SmartScreenPuaEnabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\MicrosoftEdge\PhishingFilter -Name EnabledV9 -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\MicrosoftEdge\PhishingFilter -Name PreventOverride -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter -Name EnabledV9 -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter -Name PreventOverride -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\Edge -Name SmartScreenEnabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Policies\Microsoft\Edge -Name PreventSmartScreenPromptOverride -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\Software\Policies\Microsoft\Windows Defender\SmartScreen -Name ConfigureAppInstallControl -PropertyType String -Value Anywhere -Force
    New-ItemProperty -Path HKLM:\\Software\Policies\Microsoft\Windows Defender\SmartScreen -Name ConfigureAppInstallControlEnabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost -Name EnableWebContentEvaluation -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost -Name EnableWebContentEvaluation -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Smartscreen disabled." -ForegroundColor "Green"

    # Disable key logging
    if (!(Test-Path HKCU:\SOFTWARE\Microsoft\Input)) { New-Item -Path HKCU:\SOFTWARE\Microsoft\Input -Type Folder | Out-Null }
    if (!(Test-Path HKCU:\SOFTWARE\Microsoft\Input\TIPC)) { New-Item -Path HKCU:\SOFTWARE\Microsoft\Input\TIPC -Type Folder | Out-Null }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Input\TIPC -Name Enabled -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Input\TIPC -Name Enabled -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Key logging disabled." -ForegroundColor "Green"

    # Deny access to camera
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\webcam -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to the camera denied." -ForegroundColor "Green"

    # Deny access to notifications
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\userNotificationListener -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to notifications denied." -ForegroundColor "Green"

    # Speech, Inking, & Typing: Stop "Getting to know me"
    if (!(Test-Path HKCU:\SOFTWARE\Microsoft\InputPersonalization)) { New-Item -Path HKCU:\SOFTWARE\Microsoft\InputPersonalization -Type Folder | Out-Null }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\InputPersonalization -Name RestrictImplicitTextCollection -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\InputPersonalization -Name RestrictImplicitTextCollection -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\InputPersonalization -Name AllowInputPersonalization -PropertyType DWord -Value 0 -Force
    if (!(Test-Path HKCU:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore)) { New-Item -Path HKCU:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore -Type Folder | Out-Null }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore -Name HarvestContacts -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Personalization\Settings -Name AcceptedPrivacyPolicy -PropertyType DWord -Value 0 -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy -Name HasAccepted -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Text collection disabled." -ForegroundColor "Green"

    # Deny access to account info
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\userAccountInformation -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to account informations denied." -ForegroundColor "Green"

    # Deny access to contacts
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\contacts -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to contacts denied." -ForegroundColor "Green"

    # Deny access to calendar
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\appointments -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to calendar denied." -ForegroundColor "Green"

    # Deny access to call history
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\phoneCall -Name Value -PropertyType String -Value Deny -Force
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\phoneCallHistory -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to call history denied." -ForegroundColor "Green"

    # Deny access to diagnostics
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\appDiagnostics -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to diagnostics denied." -ForegroundColor "Green"

    # Deny access to documents
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\documentsLibrary -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to documents denied." -ForegroundColor "Green"

    # Deny access to emails
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\email -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to emails denied." -ForegroundColor "Green"

    # Deny access to calendar
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\appointments -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to calendar denied." -ForegroundColor "Green"

    # Deny access to file system
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\broadFileSystemAccess -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to file system denied." -ForegroundColor "Green"

    # Deny access to location
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to location denied." -ForegroundColor "Green"

    # Deny access to messages
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\chat -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to messages denied." -ForegroundColor "Green"

    # Deny access to pictures
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\picturesLibrary -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to pictures denied." -ForegroundColor "Green"

    # Deny access to radios
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\radios -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to radios denied." -ForegroundColor "Green"

    # Deny access to tasks
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\userDataTasks -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to tasks denied." -ForegroundColor "Green"

    # Deny access to other devices
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\bluetoothSync -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to other devices denied." -ForegroundColor "Green"

    # Deny access to videos
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\videosLibrary -Name Value -PropertyType String -Value Deny -Force
    Write-Host "[+] Access to videos denied." -ForegroundColor "Green"
}

###############################################################################
### Explorer, Taskbar, and System Tray                                        #
###############################################################################
$message = "Do you want to configure Explorer, Taskbar and System Tray?"
$caption = "You can turn on some features on Explorer, Taskbar and System Tray."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring Explorer, Taskbar and System Tray..." -ForegroundColor "Cyan"
    
    # enable item check boxes
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name AutoCheckSelect -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Checkboxes enabled." -ForegroundColor "Green"

    # show hidden files
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name Hidden -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Hidden files shown." -ForegroundColor "Green"

    # show file extensions
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name HideFileExt -PropertyType DWord -Value 0 -Force
    Write-Host "[+] File extensions shown." -ForegroundColor "Green"

    # avoid creating Thumbs.db files n networks volumes
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer -Name DisableThumbnailsOnNetworkFolders -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Thumbs.db will not be created on networks volumes." -ForegroundColor "Green"

    # hide cortana
    if (Get-AppxPackage -Name Microsoft.549981C3F5F10) {
        New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name ShowCortanaButton -PropertyType DWord -Value 0 -Force
        New-ItemProperty -Path HKLM:\Software\Policies\Microsoft\Windows\Windows Search -Name AllowCortana -PropertyType DWord -Value 0 -Force
        if (-not (Test-Path -Path "Registry::HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\SystemAppData\Microsoft.549981C3F5F10_8wekyb3d8bbwe\CortanaStartupId")) {
            New-Item -Path "Registry::HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\SystemAppData\Microsoft.549981C3F5F10_8wekyb3d8bbwe\CortanaStartupId" -Force
        }
        New-ItemProperty -Path "Registry::HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\SystemAppData\Microsoft.549981C3F5F10_8wekyb3d8bbwe\CortanaStartupId" -Name State -PropertyType DWord -Value 1 -Force 
    }
    Write-Host "[+] Cortana disabled." -ForegroundColor "Green"

    # remove windows store apps
    New-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name StoreAppsOnTaskbar -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Windows Store Apps removed from the taskbar." -ForegroundColor "Green"

    # SysTray: Hide the Action Center, Network, and Volume icons
    New-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer -Name HideSCAHealth -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer -Name HideSCANetwork -PropertyType DWord -Value 1 -Force
    New-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer -Name HideSCAVolume -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Action Center, Network, and Volume icons hidden." -ForegroundColor "Green"

    # hide 3D objects folder
    if (-not (Test-Path -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{31C0DD25-9439-4F12-BF41-7FF4EDA38722}\PropertyBag")) {
        New-Item -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{31C0DD25-9439-4F12-BF41-7FF4EDA38722}\PropertyBag" -Force
    }
    New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{31C0DD25-9439-4F12-BF41-7FF4EDA38722}\PropertyBag" -Name ThisPCPolicy -PropertyType String -Value Hide -Force
    Write-Host "[+] 3D Objects folder hidden." -ForegroundColor "Green"

    # hide people button on the taskbar
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People)) {
        New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People -Name PeopleBand -PropertyType DWord -Value 0 -Force
    Write-Host "[+] People button removed from the taskbar." -ForegroundColor "Green"

    # set the default windows mode to dark
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name SystemUsesLightTheme -PropertyType DWord -Value 0 -Force
    Write-Host "[+] Windows Mode set to dark." -ForegroundColor "Green"

    # set the default app mode to dark
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme  -PropertyType DWord -Value 0 -Force
    Write-Host "[+] App Mode set to dark." -ForegroundColor "Green"

    # turn on storage sense
    if (-not (Test-Path -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy)) {
        New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy -ItemType Directory -Force
    }
    New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy -Name 01 -PropertyType DWord -Value 1 -Force
    if ((Get-ItemPropertyValue -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy -Name 01) -eq "1") {
        # run storage sense every month
        New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy -Name 2048 -PropertyType DWord -Value 30 -Force
        # delete temp files that apps aren't using
        New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy -Name 04 -PropertyType DWord -Value 1 -Force
    }
    Write-Host "[+] Storage Sense enabled." -ForegroundColor "Green"

    # disable hibernation
    POWERCFG /HIBERNATE OFF
    Write-Host "[+] Hibernation disabled." -ForegroundColor "Green"

    # disable the windows 260 chars path limit
    New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem -Name LongPathsEnabled -PropertyType DWord -Value 1 -Force
    Write-Host "[+] Windows path limit disabled." -ForegroundColor "Green"
}

###############################################################################
### Default Windows Applications                                              #
###############################################################################
$message = "Do you want to uninstall them?"
$caption = "You can uninstall some default Windows applications that are useless."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring Default Windows Applications..." -ForegroundColor "Cyan"
    
    # uninstall uwp apps
    $AppxPackages = @(
        # AMD Radeon Software
        "AdvancedMicroDevicesInc-2.AMDRadeonSoftware",

        # Intel Graphics Control Center
        "AppUp.IntelGraphicsControlPanel",
        "AppUp.IntelGraphicsExperience",

        # Sticky Notes
        "Microsoft.MicrosoftStickyNotes",

        # Screen Sketch
        "Microsoft.ScreenSketch",

        # Photos (and Video Editor)
        "Microsoft.Windows.Photos",
        "Microsoft.Photos.MediaEngineDLC",

        # HEVC Video Extensions from Device Manufacturer
        "Microsoft.HEVCVideoExtension",

        # Calculator
        "Microsoft.WindowsCalculator",

        # Windows Camera
        "Microsoft.WindowsCamera",

        # Xbox Identity Provider
        "Microsoft.XboxIdentityProvider",

        # Xbox Console Companion
        "Microsoft.XboxApp",

        # Xbox
        "Microsoft.GamingApp",
        "Microsoft.GamingServices",

        # Xbox TCUI
        "Microsoft.Xbox.TCUI",

        # Xbox Speech To Text Overlay
        "Microsoft.XboxSpeechToTextOverlay",

        # Xbox Game Bar
        "Microsoft.XboxGamingOverlay",

        # Xbox Game Bar Plugin
        "Microsoft.XboxGameOverlay",

        # NVIDIA Control Panel
        "NVIDIACorp.NVIDIAControlPanel",

        # Realtek Audio Console
        "RealtekSemiconductorCorp.RealtekAudioControl",

        # 3D Builder
        "Microsoft.3DBuilder",

        # Alarms and Clock
        "Microsoft.WindowsAlarms",

        # Autodesk Sketchbook
        "*.AutodeskSketchBook",

        # Bing Finance
        "Microsoft.BingFinance",

        # Bing News
        "Microsoft.BingNews",

        # Bing Sports
        "Microsoft.BingSports",
        'Microsoft.BingWeather',
        'king.com.BubbleWitch3Saga',
        'Microsoft.WindowsCommunicationsApps',
        'king.com.CandyCrushSodaSaga',
        '*.DisneyMagicKingdoms',
        'DolbyLaboratories.DolbyAccess',
        '*.Facebook',
        'Microsoft.MicrosoftOfficeHub',
        'Microsoft.GetStarted',
        'Microsoft.WindowsMaps',
        '*.MarchofEmpires',
        'Microsoft.Messaging',
        'Microsoft.OneConnect',
        'Microsoft.Office.OneNote',
        'Microsoft.MSPaint',
        'Microsoft.People',
        'Microsoft.Windows.Photos',
        'Microsoft.Print3D',
        'Microsoft.SkypeApp',
        '*.SlingTV',
        'Microsoft.MicrosoftSolitaireCollection',
        'SpotifyAB.SpotifyMusic',
        'Microsoft.MicrosoftStickyNotes',
        'Microsoft.Office.Sway',
        '*.Twitter',
        'Microsoft.WindowsSoundRecorder',
        'Microsoft.WindowsPhone',
        'Microsoft.XboxApp',
        'Microsoft.ZuneMusic',
        'Microsoft.ZuneVideo'
    )
    $AppxPackages | Get-AppxPackage -Name $_ -AllUsers | Remove-AppxPackage
    $AppxPackages | ForEach-Object { Get-AppxProvisionedPackage -Online | Where-Object DisplayName -Like $_ } | Remove-AppxProvisionedPackage -Online
    Write-Host "[+] UWP apps uninstalled." -ForegroundColor "Green"


    # do not let uwp apps run in the background
    # Remove all excluded apps running in the background
    Get-ChildItem -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications | ForEach-Object -Process {
        Remove-ItemProperty -Path $_.PsPath -Name * -Force
    }

    # Exclude apps from the Bundle only
    $BackgroundAccessApplications = @((Get-ChildItem -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications).PSChildName)
    $ExcludedBackgroundAccessApplications = @()
    foreach ($BackgroundAccessApplication in $BackgroundAccessApplications) {
        if (Get-AppxPackage -PackageTypeFilter Bundle -AllUsers | Where-Object -FilterScript { $_.PackageFamilyName -eq $BackgroundAccessApplication }) {
            $ExcludedBackgroundAccessApplications += $BackgroundAccessApplication
        }
    }

    Get-ChildItem -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications | Where-Object -FilterScript { $_.PSChildName -in $ExcludedBackgroundAccessApplications } | ForEach-Object -Process {
        New-ItemProperty -Path $_.PsPath -Name Disabled -PropertyType DWord -Value 1 -Force
        New-ItemProperty -Path $_.PsPath -Name DisabledByUser -PropertyType DWord -Value 1 -Force
    }

    # Open the "Background apps" page
    Start-Process -FilePath ms-settings:privacy-backgroundapps
    Write-Host "[+] Background UWP apps disabled." -ForegroundColor "Green"
}

###############################################################################
### Windows Update & Application Updates                                      #
###############################################################################
$message = "Do you want to configure Windows Update?"
$caption = "You can enable automatic updates and disable automatic reboot after install."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring Windows Update..." -ForegroundColor "Cyan"
    
    # Ensure Windows Update registry paths
    if (!(Test-Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate)) { New-Item -Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate -Type Folder | Out-Null }
    if (!(Test-Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU)) { New-Item -Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU -Type Folder | Out-Null }

    # Enable Automatic Updates
    New-ItemProperty HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU NoAutoUpdate 0
    Write-Host "[+] Enable automatic updates." -ForegroundColor "Green"

    # Disable automatic reboot after install
    New-ItemProperty HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate NoAutoRebootWithLoggedOnUsers 1
    New-ItemProperty HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU NoAutoRebootWithLoggedOnUsers 1
    Write-Host "[+] Automatic reboot disabled." -ForegroundColor "Green"

    # Configure to Auto-Download but not Install: NotConfigured: 0, Disabled: 1, NotifyBeforeDownload: 2, NotifyBeforeInstall: 3, ScheduledInstall: 4
    New-ItemProperty HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU AUOptions 3
    Write-Host "[+] Auto install disabled." -ForegroundColor "Green"

    # Include Recommended Updates
    New-ItemProperty HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU IncludeRecommendedUpdates 1
    Write-Host "[+] Include recommended updates." -ForegroundColor "Green"

    # Opt-In to Microsoft Update
    $MU = New-Object -ComObject Microsoft.Update.ServiceManager -Strict
    $MU.AddService2("7971f918-a847-4430-9279-4a52d1efe18d", 7, "") | Out-Null
    Remove-Variable MU

    # Delivery Optimization: Download from 0: Http Only [Disable], 1: Peering on LAN, 2: Peering on AD / Domain, 3: Peering on Internet, 99: No peering, 100: Bypass & use BITS
    if (!(Test-Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization)) { New-Item -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization -Type Folder | Out-Null }
    if (!(Test-Path HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\DeliveryOptimization)) { New-Item -Path HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\DeliveryOptimization -Type Folder | Out-Null }
    New-ItemProperty HKLM:\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization DODownloadMode 0
    New-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\DeliveryOptimization DODownloadMode 0
    Write-Host "[+] Disable Delivery Optimization." -ForegroundColor "Green"
}

###############################################################################
### Windows Defender                                                          #
###############################################################################
$message = "Do you want to turn off Windows Defender?"
$caption = "You can disable Windows Defender because it is useless."
if ((Get-Choice $caption $message $choices) -eq 0) {
    Write-Host "[o] Configuring Windows Defender..." -ForegroundColor "Cyan"
    
    if ((Get-MpComputerStatus).AntivirusEnabled) {
        # Disable Cloud-Based Protection: Enabled Advanced: 2, Enabled Basic: 1, Disabled: 0
        Set-MpPreference -MAPSReporting 0
        Write-Host "[+] Cloud-Based protection disabled." -ForegroundColor "Green"

        # Disable automatic sample submission: Prompt: 0, Auto Send Safe: 1, Never: 2, Auto Send All: 3
        Set-MpPreference -SubmitSamplesConsent 2
        Write-Host "[+] Sample submission disabled." -ForegroundColor "Green"

        # Enable Microsoft Defender Exploit Guard network protection
        Set-MpPreference -EnableNetworkProtection Enabled
        Write-Host "[+] Network Protection enabled." -ForegroundColor "Green"

    }
}

###############################################################################
### Spy blocker                                                               #
###############################################################################
$message = "Do you want to add firewall rules to block spies?"
$caption = "You can add firewall rules provided by WindowsSpyBlocker to block Microsoft connections."
if ((Get-Choice $caption $message $choices) -eq 0) {
    $URL = "https://github.com/crazy-max/WindowsSpyBlocker/raw/master/data/firewall/spy.txt"
    $ips = (New-Object System.Net.WebClient).DownloadString($URL).Split("`n")
    $rules = Get-NetFirewallRule
    $prefix = "SpyBlocker"

    foreach ($ip in $ips) {
        # check if firewall rule already exists
        $name = "$prefix-$ip"
        if (-not $rules.Name.Contains($name)) {
            # create it
            New-NetFirewallRule -Name $name -Direction Outbound -Protocol Any -Action Block -RemoteAddress $ip
        }
    }
    Write-Host "[+] Firewall rules added." -ForegroundColor "Green"
}

Write-Host "[+] Done." -ForegroundColor "Green"
Write-Host "[!] Note that some of these changes require a logout/restart to take effect." -ForegroundColor "Blue"