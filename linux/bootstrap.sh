#!/usr/bin/env bash
#
# boostrap creates symlinks

DOTFILES=$(pwd -P)

info () {
    printf "\r  [ \e[1;94m!\e[0m ] $1\n"
}

success () {
    printf "\r  [ \e[1;92m+\e[0m ] $1\n"
}

fail () {
    printf "\r  [ \e[1;91m-\e[0m ] $1\n"
}

link_file () {
    local src=$1 dst=$2

    local overwrite= backup= skip=
    local action=

    if [ -f "$dst" -o -d "$dst" -o -L "$dst" ]
    then

        if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]
        then

            local currentSrc="$(readlink $dst)"

            if [ "$currentSrc" == "$src" ]
            then
                skip=true;
            else
                printf "File already exists: $dst ($(basename "$src")), what do you want to do?\n\
                [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
                read -n 1 action

                case "$action" in
                    o )
                        overwrite=true;;
                    O )
                        overwrite_all=true;;
                    b )
                        backup=true;;
                    B )
                        backup_all=true;;
                    s )
                        skip=true;;
                    S )
                        skip_all=true;;
                    * )
                    ;;
                esac
            fi
        fi

        overwrite=${overwrite:-$overwrite_all}
        backup=${backup:-$backup_all}
        skip=${skip:-$skip_all}

        if [ "$overwrite" == "true" ]
        then
            rm -rf "$dst"
            success "removed $dst"
        fi

        if [ "$backup" == "true" ]
        then
            mv     "$dst" "${dst}.backup"
            success "moved $dst to ${dst}.backup"
        fi

        if [ "$skip" == "true" ]
        then
            success "skipped $src"
        fi
    fi

    if [ "$skip" != "true" ]  # "false" or empty
    then
        ln -s "$1" "$2"
        success "linked $1 to $2"
    fi
}

# install dotfiles
install_zsh () {
    info 'installing zsh'
    
    local overwrite_all=false backup_all=false skip_all=false

    files=(".zshenv" ".zshrc")
    for f in ${files[@]}; do
        dst="$HOME/$f"
        src="$DOTFILES/linux/zsh/$f"
        link_file "$src" "$dst"
    done
}

install_bat () {
    info 'installing bat'

    local overwrite_all=false backup_all=false skip_all=false

    src="$DOTFILES/config/bat.config"
    dst=$(bat --config-file)
    link_file "$src" "$dst"
}

install_starship () {
    info 'installing starship'

    local overwrite_all=false backup_all=false skip_all=false

    if [[ -z "${STARSHIP_CONFIG}" ]]; then
        dst="$HOME/.config/starship.toml"
    else
        dst="${STARSHIP_CONFIG}"
    fi
    src="$DOTFILES/config/starship.toml"
    link_file "$src" "$dst"
}

if [ -x "$(command -v zsh)" ]; then
    install_zsh
fi

if [ -x "$(command -v bat)" ]; then
    install_bat
fi

if [ -x "$(command -v starship)" ]; then
    install_starship
fi

echo ''
echo 'All installed !'