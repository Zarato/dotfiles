# ALIAS
if [ -x "$(command -v exa)" ]; then
    alias ls="exa --icons --group-directories-first"
    alias ll="exa --icons --group-directories-first -l"
else
    alias ls='ls --color=auto'
    alias ll='ls -alF'
    alias la='ls -A'
    alias l='ls -CF'
fi

if [ -x "$(command -v bat)" ]; then
    alias cat="bat --paging=never"
fi

alias grep="grep --color"

# find out which distribution we are running on
LFILE="/etc/lsb-release"
MFILE="/System/Library/CoreServices/SystemVersion.plist"
if [[ -f $LFILE ]]; then
    _distro=$(awk '/^ID=/' /etc/*-release | awk -F'=' '{ print tolower($2) }')
elif [[ -f $MFILE ]]; then
    _distro="macos"
fi

# set an icon based on the distro
case $_distro in
    *arch*)                  ICON="";;
    *debian*)                ICON="";;
    *raspbian*)              ICON="";;
    *ubuntu*)                ICON="";;
    *elementary*)            ICON="";;
    *fedora*)                ICON="";;
    *coreos*)                ICON="";;
    *gentoo*)                ICON="";;
    *mageia*)                ICON="";;
    *centos*)                ICON="";;
    *opensuse*|*tumbleweed*) ICON="";;
    *sabayon*)               ICON="";;
    *slackware*)             ICON="";;
    *linuxmint*)             ICON="";;
    *alpine*)                ICON="";;
    *aosc*)                  ICON="";;
    *nixos*)                 ICON="";;
    *devuan*)                ICON="";;
    *manjaro*)               ICON="";;
    *rhel*)                  ICON="";;
    *macos*)                 ICON="";;
    *)                       ICON="";;
esac

export STARSHIP_DISTRO="$ICON"
export ZSH="$HOME/.zsh"

# history
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=1000
export SAVEHIST=1000

# plugins
plugins=(zsh-autosuggestions zsh-syntax-highlighting)
for plugin in ${plugins[*]}; do
    # check if it is a file
    if [[ -f "$plugin" ]]; then
        source $plugin
    else
        source "$ZSH/$plugin/$plugin.zsh"
    fi
done

# autosuggestions
# source $ZSH/zsh-autosuggestions/zsh-autosuggestions.zsh

# syntax highlighting
# source $ZSH/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Load Starship
eval "$(starship init zsh)"

