function Get-Choice {
    param (
        [string] $caption,
        [string] $message,
        [string[]] $choices,
        [int] $defaultChoice = 1
    )
    return $Host.UI.PromptForChoice($caption, $message, $choices, $defaultChoice);
}

Write-Host @"
 __      _               
/ _\ ___| |_ _   _ _ __  
\ \ / _ \ __| | | | '_ \ 
_\ \  __/ |_| |_| | |_) |
\__/\___|\__|\__,_| .__/ 
                  |_|    
"@ -ForegroundColor "magenta"

$scriptsPath = Join-Path -Path $PWD -ChildPath "windows\"
$choices = @('&Yes', '&No')

# Dependencies
$caption = "In order to take advantage of some utilities and customization of windows you can install some applications."
$message = "Would you like to install them?"

$choice = Get-Choice $caption $message $choices
if ($choice -eq 0) {
    . $scriptsPath\deps.ps1
}

# Bootstrap
# verify that PSReadLine >= 2.2.0 is installed
if ($null -eq (Get-InstalledModule -Name PSReadLine -MinimumVersion 2.2.0)) {
   Install-Module PSReadLine -Force
}
$caption = "Dotfiles are written for some applications."
$message = "Do you want to create symbolic links to these dotfiles?"

$choice = Get-Choice $caption $message $choices
if ($choice -eq 0) {
    . $scriptsPath\bootstrap.ps1
}

# Windows
$caption = "Windows can be configured to have a better experience (e.g. for privacy)."
$message = "Would you like to configure windows?"

$choice = Get-Choice $caption $message $choices
if ($choice -eq 0) {
    . $scriptsPath\windows.ps1
}
