# Dotfiles

<div align="center">
    <img src="https://i.imgur.com/wrPFiEv.gif">
</div>

My personal dotfiles for `Windows` and `Linux`.

## Installation

- Windows

  ```powershell
  PS> git clone git@gitlab.com:Zarato/dotfiles.git
  PS> cd dotfiles
  PS> .\setup.ps1 # Requires administrator privilege
  ```

- Linux

  TODO

## Features

- Common

  - [bat](https://github.com/sharkdp/bat) : A cat(1) clone with wings.
  - [starship](https://github.com/starship/starship) : The minimal, blazing-fast, and infinitely customizable prompt for any shell!
  - [vscode](https://github.com/microsoft/vscode) : Visual Studio Code

- Windows

  - [Aliases](windows/aliases.ps1)
  - [Functions](windows/functions.ps1)
  - [Dependencies](windows/dependencies.ps1) : install some dependencies
  - [Windows](windows/windows.ps1) : configure for privacy, security and customisation
  - [Profile](windows/profile.ps1) : main profile file for Powershell
  - [TaskbarX](https://github.com/ChrisAnd1998/TaskbarX) : Center Windows taskbar icons with a variety of animations and options.
  - [WindowsTerminal](https://github.com/microsoft/terminal) : The new Windows Terminal and the original Windows console host, all in the same place!

- Linux

  TODO

## Roadmap

- [ ] Common
  - [x] bat
  - [x] Starship
  - [x] VSCode
  - [ ] git
  - [ ] ssh
  - [ ] Firefox
- [ ] Linux
  - [ ] fzf
  - [ ] i3
  - [ ] rofi
  - [ ] polybar
- [ ] Windows
  - [x] TaskbarX
  - [x] Windows Terminal
  - [x] Powershell
  - [ ] Lively Wallpaper
  - [ ] Flow Launcher
  - [ ] ShareX
  - [x] Security
  - [x] Privacy
